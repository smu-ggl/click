Source: click
Section: admin
Priority: optional
Maintainer: Colin Watson <cjwatson@ubuntu.com>
Standards-Version: 3.9.5
Build-Depends:
 debhelper (>= 9~),
 dbus,
 dh-autoreconf,
 dh-python,
 dh-systemd (>= 1.3),
 gir1.2-glib-2.0,
 gobject-introspection (>= 0.6.7),
 libdbustest1-dev,
 libgee-0.8-dev,
 libgirepository1.0-dev (>= 0.6.7),
 libglib2.0-dev (>= 2.34),
 libgtest-dev,
 libjson-glib-dev (>= 0.10),
 libprocess-cpp-dev,
 libproperties-cpp-dev,
 pep8,
 pkg-config,
 pyflakes,
 python3-all:any,
 python3-apt,
 python3-coverage,
 python3-debian,
 python3-gi,
 python3-pep8,
 python3-setuptools,
 python3-six,
 python3-sphinx,
 python3:any (>= 3.2),
 python3:any (>= 3.3) | python3-mock,
 valac,
Vcs-Bzr: https://code.launchpad.net/~click-hackers/click/devel
Vcs-Browser: http://bazaar.launchpad.net/~click-hackers/click/devel/files
X-Python-Version: >= 2.7
X-Python3-Version: >= 3.2
XS-Testsuite: autopkgtest

Package: click
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser,
 python3-click-package (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 click-apparmor,
Suggests:
 click-reviewers-tools (>= 0.9),
 lomiri-app-launch-tools,
Conflicts:
 click-package,
 packagekit-plugin-click,
Replaces:
 click-package,
 packagekit-plugin-click,
Provides:
 click-package,
 packagekit-plugin-click,
Description: Click packages
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package provides common files, including the main click program.

Package: click-dev
Architecture: any
Multi-Arch: foreign
Depends:
 python3-click-package (= ${binary:Version}),
 ${misc:Depends},
 ${perl:Depends},
Recommends:
 debootstrap,
 dpkg-dev,
 schroot (>= 1.6.10-2~),
Description: build Click packages
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 click-dev provides support for building these packages.

Package: python3-click-package
Section: python
Architecture: any
Depends:
 gir1.2-click-0.4 (= ${binary:Version}),
 gir1.2-glib-2.0,
 python3-apt,
 python3-debian,
 python3-gi,
 ${misc:Depends},
 ${python3:Depends},
Replaces:
 python3-click (<< 0.4.43),
Description: Click packages (Python 3 interface)
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package provides Python 3 modules used by click, which may also be
 used directly.

Package: libclick-0.4-0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: run-time Click package management library
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package provides a shared library for managing Click packages.

Package: libclick-0.4-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libclick-0.4-0 (= ${binary:Version}),
 libglib2.0-dev,
 libjson-glib-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Description: development files for Click package management library
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package provides development files needed to build programs for
 managing Click packages.

Package: gir1.2-click-0.4
Section: introspection
Architecture: any
Depends:
 libclick-0.4-0 (= ${binary:Version}),
 ${gir:Depends},
 ${misc:Depends},
Description: GIR bindings for Click package management library
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings.

Package: click-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Conflicts:
 click-package-doc,
Replaces:
 click-package-doc,
Provides:
 click-package-doc,
Description: Click packages (documentation)
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package provides documentation for click.

Package: click-service
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 click (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Click packages (privileged service)
 Click is a simplified packaging format that installs in a separate part of
 the file system, suitable for third-party applications.
 .
 This package contains click-service, a simple daemon which allows unprivileged
 users to install click packages over D-Bus.
