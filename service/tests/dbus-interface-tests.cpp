/* Copyright (C) 2021 UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "glib-thread.h"

#include <service/click-proxy.h>
#include <service/dbus-interface.h>

#include <core/posix/signal.h>
#include <core/testing/fork_and_run.h>
#include <core/testing/cross_process_sync.h>

#include <gtest/gtest.h>

#include <libdbustest/dbus-test.h>


struct DBusInterfaceTests : public ::testing::Test
{
    std::shared_ptr<GLib::ContextThread> _thread;
    std::shared_ptr<DbusTestService> _service;

    void SetUp()
    {
        auto curpath = g_getenv("PATH");
        auto path = g_strdup_printf("%s/bin:%s", SRCDIR, curpath);
        g_setenv("PATH", path, true);
        g_free(path);

        _thread = std::make_shared<GLib::ContextThread>(
            []() {},
            [this]() {  _service.reset(); }
        );

        _service = _thread->executeOnThread<std::shared_ptr<DbusTestService>>([]() {
            auto service = std::shared_ptr<DbusTestService>(
                dbus_test_service_new(nullptr),
                [](DbusTestService * service) {
                    g_clear_object(&service);
                });

            if (!service) {
                return service;
            }

            dbus_test_service_set_bus(service.get(),
                                      DBUS_TEST_SERVICE_BUS_SYSTEM);
            dbus_test_service_start_tasks(service.get());

            return service;
        });

        ASSERT_NE(nullptr, _service);
    }

    void TearDown()
    {
        _thread.reset();
    }
};

TEST_F(DBusInterfaceTests, BasicAllocation)
{
    auto dbus = new click::DBusInterface();

    EXPECT_NE(nullptr, dbus);

    delete dbus;
}

TEST_F(DBusInterfaceTests, InstallRemove)
{
    core::testing::CrossProcessSync cps;

    auto service = [this, &cps]()
    {
        auto trap = core::posix::trap_signals_for_all_subsequent_threads(
        {
            core::posix::Signal::sig_int,
            core::posix::Signal::sig_term
        });

        trap->signal_raised().connect([trap](core::posix::Signal)
        {
            trap->stop();
        });

        auto dbus = std::make_shared<click::DBusInterface>();

        dbus->ready().connect([&cps]()
        {
            cps.try_signal_ready_for(std::chrono::seconds{2});
        });

        trap->run();

        dbus.reset();

        return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure : core::posix::exit::Status::success;
    };

    auto client = [this, &cps]()
    {

        EXPECT_EQ(1u, cps.wait_for_signal_ready_for(std::chrono::seconds{2}));

        auto proxy = proxy_click_proxy_new_for_bus_sync(G_BUS_TYPE_SYSTEM,
                                                        G_DBUS_PROXY_FLAGS_NONE,
                                                        "com.lomiri.click",
                                                        "/com/lomiri/click",
                                                        nullptr,
                                                        nullptr);

        EXPECT_NE(nullptr, proxy);

        EXPECT_TRUE(proxy_click_call_install_sync(proxy,
                                                  "/tmp/foo.bar_0.0_amd64.click",
                                                  nullptr,
                                                  nullptr));

        EXPECT_TRUE(proxy_click_call_remove_sync(proxy,
                                                 "foo.bar",
                                                 nullptr,
                                                 nullptr));

        g_clear_object(&proxy);

        return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure : core::posix::exit::Status::success;
    };

    EXPECT_EQ(core::testing::ForkAndRunResult::empty,
              core::testing::fork_and_run(service, client));
}

TEST_F(DBusInterfaceTests, RemoveWithSpaceInID)
{
    core::testing::CrossProcessSync cps;

    auto service = [&cps]()
    {
        auto trap = core::posix::trap_signals_for_all_subsequent_threads(
        {
            core::posix::Signal::sig_int,
            core::posix::Signal::sig_term
        });

        trap->signal_raised().connect([trap](core::posix::Signal)
        {
            trap->stop();
        });

        auto dbus = std::make_shared<click::DBusInterface>();

        dbus->ready().connect([&cps]()
        {
            cps.try_signal_ready_for(std::chrono::seconds{2});
        });

        trap->run();

        dbus.reset();

        return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure : core::posix::exit::Status::success;
    };

    auto client = [&cps]()
    {

        EXPECT_EQ(1u, cps.wait_for_signal_ready_for(std::chrono::seconds{2}));

        auto proxy = proxy_click_proxy_new_for_bus_sync(G_BUS_TYPE_SYSTEM,
                                                        G_DBUS_PROXY_FLAGS_NONE,
                                                        "com.lomiri.click",
                                                        "/com/lomiri/click",
                                                        nullptr,
                                                        nullptr);

        EXPECT_NE(nullptr, proxy);

        GError *error = nullptr;

        EXPECT_FALSE(proxy_click_call_remove_sync(proxy,
                                                 "foo bar",
                                                 nullptr,
                                                 &error));

        EXPECT_NE(nullptr, error);
        if (error) {
            /* TODO: maybe check the error code instead, when we have it? */
            EXPECT_NE(strstr(error->message, "space character"), nullptr);
        }

        g_clear_object(&proxy);

        return ::testing::Test::HasFailure() ? core::posix::exit::Status::failure : core::posix::exit::Status::success;
    };

    EXPECT_EQ(core::testing::ForkAndRunResult::empty,
              core::testing::fork_and_run(service, client));
}
