/* Copyright (C) 2021 UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dbus-interface.h"

#include <gio/gio.h>

int main (int /* argc */, char** /* argv */)
{
  /* $PATH is not set when we are automatically started by system bus through
   * dbus activation, which causes issues when click attemps to run the
   * debsig-veerify command, and when running dpkg to do the install. We don't
   * want to override an already set $PATH though if one is set.
   */
  g_setenv("PATH",
           "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
           false);

    auto loop = g_main_loop_new(nullptr, false);
    auto dbus = std::make_shared<click::DBusInterface>();

    g_main_loop_run(loop);

    g_clear_pointer(&loop, g_main_loop_unref);

    return 0;
}
