/* Copyright (C) 2021 UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "dbus-interface.h"

#include "click-proxy.h"

#include <thread>

#include <gio/gio.h>

#include <core/signal.h>

static const gint ERROR_CLICK_BAD_ID = 2;

namespace click
{

    class DBusInterface::Impl
    {
    public:
        Impl():
            m_done(g_cancellable_new())
        {
        }

        void run()
        {
            m_thread = std::thread([this]()
            {
                auto context = g_main_context_new();
                m_loop = g_main_loop_new(context, false);
                g_main_context_push_thread_default(context);

                if (m_done != nullptr && !g_cancellable_is_cancelled(m_done))
                {
                    g_bus_own_name(G_BUS_TYPE_SYSTEM,
                                   "com.lomiri.click",
                                   G_BUS_NAME_OWNER_FLAGS_NONE,
                                   _bus_acquired_helper,
                                   _name_acquired_helper,
                                   _name_lost_helper,
                                   this,
                                   nullptr);
                }

                if (m_done != nullptr && !g_cancellable_is_cancelled(m_done))
                {
                    g_main_loop_run(m_loop);
                }

                if (m_serviceProxy != nullptr)
                {
                    g_dbus_interface_skeleton_unexport(G_DBUS_INTERFACE_SKELETON(m_serviceProxy));
                }

                g_clear_object(&m_serviceProxy);

                g_clear_object(&m_bus);
                g_clear_pointer(&m_loop, g_main_loop_unref);
                g_clear_pointer(&context, g_main_context_unref);
            });
        }

        ~Impl()
        {
            g_cancellable_cancel(m_done);
            g_clear_object(&m_done);

            if (m_loop != nullptr)
            {
                g_main_loop_quit(m_loop);
            }

            if (m_thread.joinable())
            {
                m_thread.join();
            }
        }

        void bus_acquired(GDBusConnection* bus)
        {
            if (bus == nullptr)
            {
                return;
            }

            g_debug("Bus acquired!");
            m_bus = reinterpret_cast<GDBusConnection*>(g_object_ref(bus));

            m_serviceProxy = proxy_click_skeleton_new();

            g_signal_connect(G_OBJECT(m_serviceProxy),
                             "handle-install",
                             G_CALLBACK(_install_helper),
                             this);
            g_signal_connect(G_OBJECT(m_serviceProxy),
                             "handle-remove",
                             G_CALLBACK(_remove_helper),
                             this);

            g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(m_serviceProxy),
                                             m_bus,
                                             "/com/lomiri/click",
                                             nullptr);
        }

        void name_acquired()
        {
            g_debug("Name acquired");
            m_ready();
        }

        void name_lost()
        {
            g_critical("Lost bus name!");
            g_main_loop_quit(m_loop);
        }

        bool install(GDBusMethodInvocation* invocation)
        {
            auto params = g_dbus_method_invocation_get_parameters(invocation);
            auto vpath = g_variant_get_child_value(params, 0);
            auto path = g_shell_quote(g_variant_get_string(vpath, nullptr));
            g_variant_unref(vpath);

            g_debug("Package install requested: %s", path);

            /* FIXME: Unfortunately, we must execute click to install, because
             * it still does most of the work in the Python side. We should be
             * able to move all fucntions into C++ in the future though, and
             * avoid such complexity with managing packages.
             *
             * NOTE: We use --all-users here as a simlification, though we may
             * want to support per-user package installs again in the future,
             * if we don't remove support for that outright.
             */
            auto cmd = g_strdup_printf("click install --all-users --allow-unauthenticated %s", path);
            GError* error = nullptr;
            auto result = g_spawn_command_line_sync(cmd,
                                                    nullptr,
                                                    nullptr,
                                                    nullptr,
                                                    &error);
            g_free(cmd);

            if (!result || error != nullptr)
            {
                g_dbus_method_invocation_return_error(invocation,
                                                      m_errorQuark,
                                                      1,
                                                      "Failed to install '%s'",
                                                      path);
                g_clear_error(&error);
            }
            else
            {
                g_dbus_method_invocation_return_value(invocation, nullptr);
            }
            g_free(path);

            return true;
        }

        bool remove(GDBusMethodInvocation* invocation)
        {
            auto params = g_dbus_method_invocation_get_parameters(invocation);
            auto vname = g_variant_get_child_value(params, 0);
            std::string id{g_variant_get_string(vname, nullptr)};
            g_variant_unref(vname);

            g_debug("Package removal requested: %s", id.c_str());

            /* As click packages do now allow space character in the name,
             * we need to check it before the invocation.
             */
            GError* error = nullptr;
            gboolean result = false;

            if (id.find(' ') == std::string::npos)
            {
                std::string cmd{"click unregister --all-users " + id};

                result = g_spawn_command_line_sync(cmd.c_str(),
                                                        nullptr,
                                                        nullptr,
                                                        nullptr,
                                                        &error);
            }
            else
            {
                g_dbus_method_invocation_return_error(
                    invocation,
                    m_errorQuark,
                    /* code */ ERROR_CLICK_BAD_ID,
                    "Click packages cannot have space characters in their names!");

                return true;
            }

            if (!result || error != nullptr)
            {
                g_dbus_method_invocation_return_error(invocation,
                                                      m_errorQuark,
                                                      1,
                                                      "Failed to remove '%s'",
                                                      id.c_str());
                g_clear_error(&error);
            }
            else
            {
                g_dbus_method_invocation_return_value(invocation, nullptr);
            }

            return true;
        }

        core::Signal<>& ready()
        {
            return m_ready;
        }

    private:
        static void _bus_acquired_helper(GDBusConnection* bus,
                                         const gchar*,
                                         gpointer data)
        {
            static_cast<Impl*>(data)->bus_acquired(bus);
        }

        static void _name_acquired_helper(GDBusConnection*,
                                          const gchar*,
                                          gpointer data)
        {
            static_cast<Impl*>(data)->name_acquired();
        }

        static void _name_lost_helper(GDBusConnection*,
                                      const gchar*,
                                      gpointer data)
        {
            static_cast<Impl*>(data)->name_lost();
        }

        static gboolean _install_helper(proxyClick*,
                                        GDBusMethodInvocation* invocation,
                                        gpointer data)
        {
            return static_cast<Impl*>(data)->install(invocation);
        }

        static gboolean _remove_helper(proxyClick*,
                                       GDBusMethodInvocation* invocation,
                                       gpointer data)
        {
            return static_cast<Impl*>(data)->remove(invocation);
        }

        /* Signal that our service worker thread is ready */
        core::Signal<> m_ready;

        /* Our thread object */
        std::thread m_thread;

        /* GLib types */
        GDBusConnection* m_bus = nullptr;
        GMainLoop* m_loop = nullptr;
        GCancellable* m_done = nullptr;
        proxyClick* m_serviceProxy = nullptr;
        const GQuark m_errorQuark = g_quark_from_static_string("dbus-interface-impl");
    };


    DBusInterface::DBusInterface():
        p(new Impl())
    {
        p->run();
    }

    DBusInterface::~DBusInterface()
    {
    }

    core::Signal<>& DBusInterface::ready()
    {
        return p->ready();
    }

} // click
